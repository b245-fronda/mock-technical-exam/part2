let collection = [];

// Write the queue functions below.

function print() {
    return collection;
    // It will show the array
}

function enqueue(element) {
    collection[collection.length] = element;
    return collection;
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method
}

function dequeue() {
    let newArr = [];
    for(i = 1; i < collection.length; i++){
        newArr[newArr.length] = collection[i]
    }
    collection = newArr;
    return collection;
    // In here you are going to remove the first element in the array
}

function front() {
    return collection[0];
    // In here, you are going to get the first element
}

// starting from here, di na pwede gumamit ng .length property
function size() {
    let count = 0;
    for(let i = 0; i < collection.length; i++){
        count++
    }
    return count;
    // Number of elements
}

function isEmpty() {
    if(collection.length === 0){
        return true;
    }else{
        return false;
    }
    //it will check whether the function is empty or not
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};